<?php
/*
	Plugin Name: WP Test Floating Menu
	Plugin URI: http://www.sampledomain.com
	Description: This is a simple and basic plugin to show fix floating menu in WordPress dashboard.
	Version: 1.0.0
	Author: Bhavna Sharma
	Author URI: http://www.sampledomain.com
*/

/**** Create Text Domain ****/
function tools_submenu_load_textdomain() {
  load_plugin_textdomain( 'floating-menu', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}
add_action( 'plugins_loaded', 'tools_submenu_load_textdomain' );
 
if ( ! defined( 'ABSPATH' ) ) exit;

define( 'TOOLS_SUB_MENU_EXTENSION_SLUG' , 'floating-menu' );
define( 'TOOLS_SUB_MENU_EXTENSION_VER' , '1.0.0' );
define( 'TOOLS_SUB_MENU_EXTENSION_DIR' , plugin_dir_path( __FILE__ ) );
define( 'TOOLS_SUB_MENU_EXTENSION_URI' , plugin_dir_url( __FILE__ ) );

class ToolsSubMenuSettingPage{
	/**
	 * Holds the values to be used in the fields callbacks
	 */
	private $options;

	/**
	 * Start up
	 */
	public function __construct(){
		add_action('admin_enqueue_scripts', 'tools_sub_menu_admin_enqueue_scripts' );
		add_action( 'admin_menu', array( $this, 'toolssubmenu_plugin_page' ) );
    	add_action( 'admin_head', array( $this, 'toolssubmenu_page_init') );
	}

	/**
	 * Add plugin page
	 */
	public function toolssubmenu_plugin_page(){
		// This page will be in "Tools Sub Menu"
		add_submenu_page(
	        'tools.php',
	        __('Settings Admin', TOOLS_SUB_MENU_EXTENSION_SLUG),
			__('Floating Menu', TOOLS_SUB_MENU_EXTENSION_SLUG),
	        'manage_options',
	        'floatingmenu',
	        array( $this, 'toolssubmenu_admin_page' )
	    );
	}

	/**
	 * Show Fixed Floating Bar
	 */
	public function toolssubmenu_page_init(){ 
		$screen = get_current_screen(); ?>
		<div class="wrap">
			<div id="floating-menu" style="display: none;">
				<div class="menu pmd-floating-action" role="navigation"> 
			        <input type="hidden" class="currscreenName" name="currscreenName" value="<?php echo $screen->id; ?>">
			        <a href="javascript:void(0);" id="tour-guide" class="pmd-floating-action-btn btn pmd-btn-fab pmd-btn-raised pmd-ripple-effect btn-primary" data-title="Guide" data-tour> 
			            <span class="pmd-floating">Guide</span>
			        </a>
			    </div>
			</div>
		</div>	
	<?php }

	/**
	 * Plugin page callback
	 */
	public function toolssubmenu_admin_page(){
		// Set class property
		$this->options = get_option( 'toolssubmenu_options' );	?>
		<div class="wrap">
			<h2><?php _e('Floating Menu', TOOLS_SUB_MENU_EXTENSION_SLUG); ?></h2>			
		</div>
	<?php
	}

}

//ENQUEUE ADMIN SCRIPTS
if( !function_exists('tools_sub_menu_admin_enqueue_scripts') ){
    /**
     * Register scripts for admin panel
     * @return void
     */
    function tools_sub_menu_admin_enqueue_scripts(){
		wp_enqueue_script('jquery-js', '//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js','','', true);
		wp_enqueue_script('propeller-global-admin-script', '//propeller.in/components/global/js/global.js','','', true);
		wp_enqueue_script('propeller-rippleeffect-admin-script', '//propeller.in/components/button/js/ripple-effect.js','','', true);

		wp_enqueue_script('floating-menu-bootstrap-min', TOOLS_SUB_MENU_EXTENSION_URI . 'assets/admin/js/bootstrap.min.js','','', true);
		wp_enqueue_script('floating-menu-jquery-smoothscroll', TOOLS_SUB_MENU_EXTENSION_URI . 'assets/admin/js/jquery.smoothscroll.js','','', true);
		wp_enqueue_script('floating-menu-bootstrap-tour', TOOLS_SUB_MENU_EXTENSION_URI . 'assets/admin/js/bootstrap-tour.js','','', true);
		wp_enqueue_script('floating-menu-bootstrap-tour-docs', TOOLS_SUB_MENU_EXTENSION_URI . 'assets/admin/js/bootstrap-tour.docs.js','','', true);
		wp_enqueue_script('floating-menu-admin-script', TOOLS_SUB_MENU_EXTENSION_URI . 'assets/admin/js/admin-custom.js','','', true);
		
		wp_enqueue_style( 'admin-floating-icon-font', '//fonts.googleapis.com/icon?family=Material+Icons', false);
		
		wp_enqueue_style( 'admin-floating-bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css', false);
		wp_enqueue_style( 'admin-floating-bootstrap-tour', TOOLS_SUB_MENU_EXTENSION_URI . 'assets/admin/css/bootstrap-tour.css', false);
		wp_enqueue_style( 'admin-floating-menu-stylesheet', TOOLS_SUB_MENU_EXTENSION_URI . 'assets/admin/css/admin-custom.css', false);
    }
}

/**
 * Calls the class on the page edit screen.
 */
function wordpress_toolssubmenu(){
    new ToolsSubMenuSettingPage();
}

if ( is_admin() ) {
    $floatingMenu = new ToolsSubMenuSettingPage();
    add_action( 'load-post.php', 'wordpress_toolssubmenu' );
    add_action( 'load-post-new.php', 'wordpress_toolssubmenu' );
}