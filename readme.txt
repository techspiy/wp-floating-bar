﻿=== WP Test Floating Menu ===
Contributors: Bhavna Sharma
Tags: sample, tools, floating menu
Requires at least: 3.4.
Tested up to: 4+
Stable tag: 1.0
License: MIT

This is a simple and basic plugin to show fix floating menu in WordPress dashboard.

== Description ==

This is a simple and basic plugin to show fix floating menu in WordPress dashboard.

== Installation ==

1. Upload the WordPress Tools Menu plugin to your WP site.
2. Activate it

== Changelog ==

==== 1.0 ====
* Intial Release
