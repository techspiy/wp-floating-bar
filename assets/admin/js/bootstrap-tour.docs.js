(function() {
  $(function() {

    // Get current page screen name.
    var currscreenNameVal = $('.currscreenName').val();

    // Get json data from API for specific WP screen.
    var resturl = "http://jinelix.co.il/wpplugin/steps.php?currScreenName="+currscreenNameVal+"";

    // Get Json data from url and pass it on tour guide function.
    $.getJSON( resturl, function( data ) {
      takeTourFunc(data);
    });

    // Function to create bootstrap tour guide with data fetching from API.
    function takeTourFunc(data) {
      var $wpGuide, duration, remaining, tour;
      $wpGuide = $("#tour-guide");
      duration = 5000;
      remaining = duration;
      tour = new Tour({
        onStart: function() {
          return $wpGuide.addClass("disabled", true);
        },
        onEnd: function() {
          return $wpGuide.removeClass("disabled", true);
        },
        debug: true,
        steps: data
      }).init();

      // Verify if the tour ended.
      if (tour.ended()) {
        $('<div class="alert alert-info alert-dismissable"><button class="close" data-dismiss="alert" aria-hidden="true">&times;</button>You ended the demo tour. <a href="#" data-tour>Restart the demo tour.</a></div>').prependTo(".content").alert();
      }
      
      // Click Function Of Guide Button
      $(document).on("click", "[data-tour]", function(e) {
        e.preventDefault();
        if ($(this).hasClass("disabled")) {
          return;
        }
        tour.restart();
        return $(".alert").alert("close");
      });
    }

    $("html").smoothScroll();
  });

}).call(this);
